﻿namespace ToDoListik.Model
{
    public class ToDoItem
    {
        public int Id { get; set; }

        public bool Done { get; set; }

        public string Name { get; set; } = string.Empty;


        //Внешний ключ (и так бы создавался в таблице, т.к. в ToDoList находится список ToDoItemов)
        public int ToDoListId { get; set; }
        //Навигационное свойство (не может быть пустым - так как элемент списка без списка не существует)
        public ToDoList ToDoList { get; set; } = null!;
    }
}
