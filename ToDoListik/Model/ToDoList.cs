﻿namespace ToDoListik.Model
{
    //В перспективе можно заменить доступ из сервиса напрямую к классам на интерфейсы, для универсальности модели
    public class ToDoList
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
    }
}
