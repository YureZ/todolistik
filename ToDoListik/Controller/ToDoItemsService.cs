﻿using Microsoft.EntityFrameworkCore;
using ToDoListik.Model;

namespace ToDoListik.Controller
{
    public class ToDoItemsService
    {
        private readonly ToDoBaseContext _context;

        public ToDoItemsService(ToDoBaseContext context)
        {
            _context = context;
        }


        public async Task<List<ToDoItem>> GetAllItemsFromList(ToDoList toDoList)
        {
            return await _context.ToDoItems.Where(x => x.ToDoList.Id == toDoList.Id).ToListAsync();
        }


        public async Task CreateToDoItem(ToDoItem toDoItem)
        {
            _context.Add(toDoItem);
            await _context.SaveChangesAsync();
        }

        public async Task<ToDoItem> ReadToDoItem(int id)
        {
            return await _context.FindAsync<ToDoItem>(id) ?? new ToDoItem();
        }

        public async Task UpdateToDoItem(ToDoItem toDoItem)
        {
            _context.Update(toDoItem);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteToDoItem(ToDoItem toDoItem)
        {
            _context.Remove(toDoItem);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> ToDoItemExists(int id)
        {
            return await _context.ToDoItems.AnyAsync(e => e.Id == id);
        }

    }
}
