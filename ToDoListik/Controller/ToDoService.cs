﻿using Microsoft.EntityFrameworkCore;
using ToDoListik.Model;

namespace ToDoListik.Controller
{
    public class ToDoService
    {
        private readonly ToDoBaseContext _context;

        public ToDoService(ToDoBaseContext context)
        {
            _context = context;
        }
    

        public async Task<List<ToDoList>> GetAllToDoLists()
        {
            //Подтягиваем вместе со списками их содержимое
            return await _context.ToDoLists.Include(x => x.ToDoItems).ToListAsync();
        }

        public async Task LoadAllToDoLists()
        {
            await _context.ToDoLists.LoadAsync();
        }



        public async Task CreateToDoList(ToDoList toDoList)
        {
             _context.Add(toDoList);
             await _context.SaveChangesAsync();
        }

        //Можно было бы использовать комбинацию сервисов на фронте, но это типичная ситуация добавления вложенного элемента (можно сделать и удаление)
        public async Task CreateToDoItemInList(ToDoList toDoList, ToDoItem toDoItem)
        {
            //Находим список в который мы добавляем новую задачу и добавляем её
            //(первичный, внешний ключ и навигационное свойство генерируются сами)
            _context.ToDoLists.Find(toDoList.Id)?.ToDoItems.Add(toDoItem);
            await _context.SaveChangesAsync();
        }

        public async Task<ToDoList> ReadToDoList(int id)
        {
             return await _context.ToDoLists.Include(x => x.ToDoItems).FirstOrDefaultAsync(y => y.Id == id) ?? new ToDoList();
        }

        public async Task UpdateToDoList(ToDoList toDoList)
        {
            _context.Update(toDoList);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteToDoList(ToDoList toDoList)
        {
            _context.Remove(toDoList);
            await _context.SaveChangesAsync();
        }



        public async Task<bool> ToDoListExists(int id)
        {
            return await _context.ToDoLists.AnyAsync(e => e.Id == id);
        }

        public ToDoList Clone(ToDoList toDoList)
        {
            return new ToDoList() { Id = toDoList.Id, Name = toDoList.Name, ToDoItems = toDoList.ToDoItems.ToList() };
        }

    }
}
