﻿using Microsoft.EntityFrameworkCore;
using ToDoListik.Model;

namespace ToDoListik.Controller
{
    public class ToDoBaseContext : DbContext
    {
        public string DbPath { get; }


        public DbSet<ToDoItem> ToDoItems { get; set; } = null!;

        public DbSet<ToDoList> ToDoLists { get; set; } = null!;


        public ToDoBaseContext()
        {
            //var folder = Environment.SpecialFolder.LocalApplicationData;
            //var path = Environment.GetFolderPath(folder);
            var path = Directory.GetCurrentDirectory();
            DbPath = System.IO.Path.Join(path, "todos.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        { 
            options
                .EnableSensitiveDataLogging()
                .UseSqlite($"Data Source={DbPath}");
        }


    }
}
